<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;

class PostController extends Controller
{

    protected $limit = 4;

    public function index()
    {

      
      $posts = Post::with('author')->latest()->simplePaginate($this->limit);
      $categories = Category::with('posts')->orderBy('title', 'asc')->get();
      return view('blog.index', compact('posts', 'categories'));
    }


    public function show(Post $post)
    {
      $categories = Category::with('posts')->orderBy('title', 'asc')->get();
      return view('blog.show', compact('post', 'categories'));
    }

    public function category(Category $category)
    {

      $categoryName = $category->title;
      $posts = $category->posts()->with('author')->latest()->simplePaginate($this->limit);
      $categories = Category::with('posts')->orderBy('title', 'asc')->get();
      return view('blog.index', compact('posts', 'categories', 'categoryName'));
    }
}
