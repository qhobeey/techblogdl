<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{

    public function createLogin()
    {
      return view('blog.show-login');
    }

    public function login()
    {

      $this->validate(request(), [
        'email' => 'required',
        'password' => 'required'
      ]);

      if(!auth()->attempt(request(['email', 'password']))):
        return redirect()->back();
      endif;

      return redirect()->route('admin.index');
    }

    public function logout()
    {
      auth()->logout();
      return redirect()->route('index');
    }
}
