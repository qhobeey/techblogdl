<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Category;
use Image;

class AdminPostController extends Controller
{

  protected $limit = 4;

  public function index()
  {
    $posts = Post::with('author')->latest()->simplePaginate($this->limit);
    return view('backend.index', compact('posts'));
  }

  public function show(Post $post)
  {
    return view('backend.show', compact('post'));
  }

  public function category()
  {

  }

  public function create()
  {
    $categories = Category::with('posts')->orderBy('title', 'asc')->get();
    return view('backend.create',compact('categories'));
  }

  public function store()
  {
    $this->validate(request(), [
      'title' => 'required|min:5',
      'excerpt' => 'required|min:20',
      'body' => 'required|min:50'

    ]);

    if(request()->hasFile('image')):
      $image = request()->file('image');
      $filename = time() . "." . $image->getClientOriginalExtension();
      Image::make($image)->resize(750, 425)->save(public_path('img/uploads/posts/' . $filename));

      Post::create([
        'title' => request('title'),
        'slug' => str_replace(" ","-",strtolower(request('title'))),
        'category_id' => request('category'),
        'excerpt' => request('excerpt'),
        'body' => request('body'),
        'author_id' => 1,
        'image' => $filename
      ]);

    else:
      Post::create([
        'title' => request('title'),
        'slug' => str_replace(" ","-",strtolower(request('title'))),
        'category_id' => request('category'),
        'excerpt' => request('excerpt'),
        'body' => request('body'),
        'author_id' => 1
      ]);

    endif;

    return redirect()->route('admin.index');
  }

  public function createCategory()
  {
    return view('backend.create-category');
  }

  public function storeCategory()
  {
    $this->validate(request(), [
      'title' => 'required',
      'slug' => 'required|alpha_dash'
    ]);

    Category::create([
      'title' => request('title'),
      'slug' => request('slug'),
    ]);
    return redirect()->route('admin.index');
  }

}
