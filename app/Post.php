<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GrahamCampbell\Markdown\Facades\Markdown;

class Post extends Model
{

    protected $guarded = [];

    public function getImageUrlAttribute($value)
    {
      $imageUrl = "";
      if( ! is_null($this->image)):
        $imagePath = public_path() . "/img/uploads/posts/". $this->image;
        if(file_exists($imagePath)) $imageUrl = asset("img/uploads/posts/". $this->image);
      endif;

      return $imageUrl;
    }

    public function author()
    {
      return $this->belongsTo(User::class);
    }

    public function getDateAttribute($value)
    {
      return $this->created_at->diffForHumans();
    }

    public function getBodyHtmlAttribute($value)
    {
      return $this->body ? Markdown::convertToHtml($this->body) : NULL;
    }

    public function getExcerptHtmlAttribute($value)
    {
      return $this->excerpt ? Markdown::convertToHtml($this->excerpt) : NULL;
    }

    public function category()
    {
      return $this->belongsTo(Category::class);
    }

    public function getSlugAttribute()
    {
      return str_replace(" ","-",$this->title);
    }


}
