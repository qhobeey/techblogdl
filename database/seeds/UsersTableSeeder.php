d<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('users')->truncate();
        DB::table('users')->insert([
            [
              'name' => 'Qhobeey Martison',
              'email' => 'qhobeey@gmail.com',
              'password' => bcrypt('password')
            ],
            [
              'name' => 'David Bludoh',
              'email' => 'david@gmail.com',
              'password' => bcrypt('password')
            ],
            [
              'name' => 'Henry Martison',
              'email' => 'henry@gmail.com',
              'password' => bcrypt('password')
            ],
        ]);
    }
}
