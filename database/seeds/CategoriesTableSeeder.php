<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->truncate();

        DB::table('categories')->insert([
          [
            'title' => 'Web Development',
            'slug' => 'web-development'
          ],
          [
            'title' => 'Web Designs',
            'slug' => 'web-designs'
          ],
          [
            'title' => 'Social media Marketing',
            'slug' => 'social-media-marketing'
          ],
          [
            'title' => 'SEO',
            'slug' => 'seo'
          ],
          [
            'title' => 'Internet',
            'slug' => 'internet'
          ],
        ]);

    }
}
