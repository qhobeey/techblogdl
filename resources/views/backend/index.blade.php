@extends('layouts.backend.master')

@section('main')
    @if( ! $posts->count())
      <div class="alert alert-info">
        <p>Nothing Found</p>
      </div>
    @else
        @if( isset($categoryName))
          <div class="alert alert-info">
            <p>Category: <strong>{{ $categoryName}}</strong></p>
          </div>
        @endif

        @foreach($posts as $post)
          <article class="post-item">
            @if($post->image_url)
                <div class="post-item-image">
                    <a href="{{ route('admin.show', $post->slug)}}">
                        <img src="{{ $post->image_url}}" class="img-responsive" alt="">
                    </a>
                </div>
              @endif
              <div class="post-item-body">
                  <div class="padding-10">
                      <h2><a href="{{ route('admin.show', $post->slug)}}">{{ $post->title }}</a></h2>
                      <p>{!! $post->excerpt_html !!}</p>
                  </div>

                  <div class="post-meta padding-10 clearfix">
                      <div class="pull-left">
                          <ul class="post-meta-group">
                              <li><i class="fa fa-user"></i><a href="#"> {{ $post->author->name}}</a></li>
                              <li><i class="fa fa-clock-o"></i><time> {{ $post->date}}</time></li>
                              <li><i class="fa fa-folder"></i><a href="{{ route('category', $post->category->slug)}}"> {{$post->category->title}}</a></li>
                              <li><i class="fa fa-comments"></i><a href="#">4 Comments</a></li>

                          </ul>
                      </div>
                      <div class="pull-right">
                          <a href="{{ route('admin.show', $post->slug)}}">Continue Reading &raquo;</a>
                      </div>
                  </div>
              </div>
          </article>
        @endforeach

    @endif

    <nav>
    {{ $posts->links()}}
    </nav>
@endsection
