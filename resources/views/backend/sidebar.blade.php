<div class="row ad-sidebar">
  <ul class="nav nav-stacked">
    <li role="presentation" class="active"><a href="#">Dashboard</a></li>
    <li role="presentation"><a href="{{route('admin.post.create')}}">Create a Post</a></li>
    <li role="presentation"><a href="{{route('admin.category.create')}}">Add a Category</a></li>
    <li role="presentation"><a href="#">Add a User</a></li>
    <li role="presentation"><a href="#">Settings</a></li>
  </ul>
</div>
