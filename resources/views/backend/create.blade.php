@extends('layouts.backend.master')

@section('main')
    <div class="row admn-frm">
      <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ route('admin.post.create')}}">
        {{csrf_field()}}
          <div class="form-group required">
              <label for="name">title</label>
              <input type="text" name="title" id="name" class="form-control">
          </div>
          <div class="form-group required">
              <label for="name">Category</label>
              <select class="form-control" name="category">
                @foreach($categories as $category)
                  <option value="{{$category->id}}">{{$category->title}}</option>
                @endforeach
              </select>
          </div>
          <div class="form-group">
              <label for="image">Upload Image</label>
              <input type="file" name="image" id="image" class="form-control">
          </div>
          <div class="form-group required">
              <label for="excerpt">Excerpt</label>
              <textarea name="excerpt" id="excerpt" rows="4" class="form-control"></textarea>
          </div>
          <div class="form-group required">
              <label for="body">Body</label>
              <textarea name="body" id="body" rows="15" class="form-control"></textarea>
          </div>
          <div class="clearfix">
              <div class="pull-left">
                  <button type="submit" class="btn btn-lg btn-success">Submit</button>
              </div>
              <div class="pull-right">
                  <p class="text-muted">
                      <span class="required">*</span>
                      <em>Indicates required fields</em>
                  </p>
              </div>
          </div>
      </form>
    </div>
@endsection
