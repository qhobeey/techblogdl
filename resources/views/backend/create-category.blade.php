@extends('layouts.backend.master')

@section('main')
    <div class="row admn-frm">
      <form class="form-horizontal" method="post" action="{{ route('admin.category.create')}}">
        {{csrf_field()}}
          <div class="form-group required">
              <label for="name">title</label>
              <input type="text" name="title" id="name" class="form-control">
          </div>
          <div class="form-group">
              <label for="slug">Slug</label>
              <input type="text" name="slug" id="slug" class="form-control">
          </div>
          <div class="clearfix">
              <div class="pull-left">
                  <button type="submit" class="btn btn-lg btn-success">Submit</button>
              </div>
              <div class="pull-right">
                  <p class="text-muted">
                      <span class="required">*</span>
                      <em>Indicates required fields</em>
                  </p>
              </div>
          </div>
      </form>
    </div>
@endsection
