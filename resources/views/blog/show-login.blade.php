@extends('layouts.master')

@section('main')
  <div class="row login-frm">
    <form class="form-horizontal" method="post" action="{{ route('login')}}">
      {{csrf_field()}}
        <div class="form-group required">
            <label for="email">Email</label>
            <input type="text" name="email" id="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="password">password</label>
            <input type="password" name="password" id="slug" class="form-control">
        </div>
        <div class="clearfix">
            <div class="pull-left">
                <button type="submit" class="btn btn-lg btn-success">login</button>
            </div>
            <div class="pull-right">
                <p class="text-muted">
                    <span class="required">*</span>
                    <em>Indicates required fields</em>
                </p>
            </div>
        </div>
    </form>
  </div>
@endsection
