<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">
    <title>techBlogdl | Tech blog</title>

    <link href='https://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/custom.css">
</head>
<body>
    @include('layouts.nav')

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                @yield('main')
            </div>
            <div class="col-md-4">
                @yield('sidebar')
            </div>
        </div>
    </div>

    @include('layouts.footer')

    <script src="/js/bootstrap.min.js"></script>
</body>
</html>
