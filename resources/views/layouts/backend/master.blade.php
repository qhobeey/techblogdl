<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">
    <title>techBlogdl | Admin</title>

    <link href='https://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/custom.css">
    <link rel="stylesheet" href="/css/admin.css">
</head>
<body>
    @include('layouts.backend.nav')

    <div class="container-fluid">
        <div class="row pd">
            <div class="col-md-2">
              @include('backend.sidebar')
            </div>
            <div class="col-md-10 ad-main">
              @yield('main')
            </div>
        </div>
    </div>

    @include('layouts.footer')

    <script src="/js/bootstrap.min.js"></script>
</body>
</html>
