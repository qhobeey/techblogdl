<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'guest'], function(){
  Route::get('/', 'PostController@index')->name('index');
  Route::get('show/{post}', 'PostController@show')->name('blog.show');
  Route::get('category/{category}', 'PostController@category')->name('category');
  Route::get('admin/login', 'Backend\AdminController@createLogin')->name('login');
  Route::post('admin/login', 'Backend\AdminController@login');
});
Route::group(['middleware' => 'auth'], function(){
  Route::get('admin', 'Backend\AdminPostController@index')->name('admin.index');
  Route::get('admin/show/{post}', 'Backend\AdminPostController@show')->name('admin.show');
  Route::get('admin/post/create', 'Backend\AdminPostController@create')->name('admin.post.create');
  Route::post('admin/post/create', 'Backend\AdminPostController@store');
  Route::get('admin/category/create', 'Backend\AdminPostController@createCategory')->name('admin.category.create');
  Route::post('admin/category/create', 'Backend\AdminPostController@storeCategory');
  Route::get('logout', 'Backend\AdminController@logout')->name('logout');
});
